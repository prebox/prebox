<?php

$query = "
CREATE TABLE `prebox`.`sim` ( 
	`id` INT NOT NULL AUTO_INCREMENT , 
	`carrierId` INT NOT NULL , 
	`code` VARCHAR(64) NOT NULL , 
	`sim` VARCHAR(64) NOT NULL , 
	`status` VARCHAR(15) NOT NULL , 
	`groupId` INT NOT NULL , 
	`usedAt` INT NOT NULL , 
	`createdAt` INT NOT NULL , 
	`updatedAt` INT NOT NULL , 
	`deleted` BOOLEAN NOT NULL , 
	PRIMARY KEY (`id`)
) ENGINE = InnoDB;

";

?>
