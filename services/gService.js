angular.module('services.global', [])

.service('gService', function($q, $http, webService) {
  return {
    /*************** COMMON OPERATIONS ***************/
    formatPhone: function(str) {
      if(!str) return "";
      return str.toString().replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
    },
    formatMoney: function(number) {
      if(!number) number = 0;
      return "$" + Number(number).toFixed(2);
    },
    validatePhone: function(phone) {
      if(!phone) return false;
      var re = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/
      return re.test(phone);
    },
    validateEmail: function(email) {
      if(!email) return false;
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },

    getCookie: function(name) {
      var re = new RegExp(name + "=([^;]+)");
      var value = re.exec(document.cookie);
      return (value != null) ? unescape(value[1]) : null;
    },
    setCookie: function(name,value,days) {
      if (days) {
          var date = new Date();
          date.setTime(date.getTime() + (days*24*60*60*1000));
          var expires = "; expires=" + date.toUTCString();
      }
      else var expires = "";
      document.cookie = name + "=" + value + expires + "; path=/";
    },
    removeCookie: function(name) {
        this.setCookie(name,"",-1);
    },
    
    lookup: function(arr, id) {
      for(var i=0; i<arr.length; i++) {
        if(arr[i].id == id)
          return arr[i];
      }
      return false;
    },

    parseGroup: function(str, index) {
      if(str) {
        tmp = JSON.parse(str);
        num = tmp.length;
        if(!index) index = 0;
        if(index + 1 > num)
          return false;
        else
          return tmp[index];
      }
      return false;
    },
    
    getGroup: function(groupId) {
      for(var i in gCurrent.groups) {
        if(gCurrent.groups[i].groupId == groupId)
          return gCurrent.groups[i];
      } 
      return false;
    },

    getCurrentGroup: function() {
      if(gCurrent.group.groupId == gCurrent.mygroup.groupId)
        return gCurrent.mygroup;
      else
        return this.getGroup(gCurrent.group.groupId);
    },

    /*
    getLocalObj: function(name) {
      var result = localStorage.getItem(name);
      try {
        if (result) result = JSON.parse(result);
        else result = [];
        return result;
      } catch(err) {
        return [];
      }
    },
    setLocalObj: function(name, obj) {
      try {
        localStorage.setItem(name, JSON.stringify(obj));
        return true;
      }
      catch(err) {
        return false;
      }
    },
    getLocalArr: function(name, index) {
      var result = localStorage.getItem(name);
      try {
        if (result) result = JSON.parse(result);
        return result[index - 1];
      } catch(err) {
        return false;
      }
    },
    deleteLocalArr: function(name, index) {
      var arr = this.getLocalObj(name);
      if(!arr) arr = [];
      try {
        if(index > 0) {
          arr.splice(index - 1, 1);
          localStorage.setItem(name, JSON.stringify(arr));
          return true;
        }  else 
          return false;
      } catch(err) {
        return false;
      }
    },
    insertLocalArr: function(name, obj, index) {
      var arr = this.getLocalObj(name);
      if(!arr) arr = [];
      try {
        if(!arr || !arr.length) arr = [];
        if(index > 0) {
          arr.splice(index - 1, 1);
          arr.splice(index - 1, 0,  obj);
        }
        else {
          arr.push(obj);
          index = arr.length;
        }
        localStorage.setItem(name, JSON.stringify(arr));
        return index;
      } catch(err) {
        return false;
      }
    },
    */
    
    getZipCoordinates: function(zip, callback) {
      webService.postData(gConfig.service.baseUrl + 'account/zipCoordinates', {zip: zip}).then(function(result) {
        callback(result);
      });
    },
    calculateDistance: function(lat1, lng1, lat2, lng2) {
        var a = lat1 - lat2;
        var b = lng1 - lng2;
        a = a * a;
        b = b * b;
        var d = Math.sqrt(a + b);
        return d * 62.137;
    }
  }
})
