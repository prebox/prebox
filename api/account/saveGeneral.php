<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/addressFunctions.php");
include_once(dirname(__FILE__)."/../_lib/groupFunctions.php");

$setStr = "
		accountId = '$accountId',
		name = '".addslashes($name)."',
		groupType = '$groupType',
		status = '$status',
		taxId = '$taxId',
		formatted = '".addslashes($formatted)."',
		addressId = '$addressId',
		contactName = '".addslashes($contactName)."',
		email = '".addslashes($email)."',
		mobile = '$mobile'
";
$success = false;

// EDIT
if($groupId) {
	$query = "UPDATE groups SET $setStr, updatedAt ='$now' WHERE groupId = '$groupId'";
	mysqli_query($link, $query);
} 

// ADD
else if($parentId) {
	$parent = getGroup($parentId);

	// ADJUST l and r BEFORE insert
	$query1 = "UPDATE groups set r=r+2 where groupType='".$parent['groupType']."' and r >= ".$parent[r];
	mysqli_query($link, $query1);
	$query2 = "UPDATE groups set l=l+2 where groupType='".$parent['groupType']."' and l >= ".$parent[r];
	mysqli_query($link, $query2);

	$query = "INSERT INTO groups SET $setStr, createdAt = '$now', l='".$parent['r']."', r='".($parent['r'] + 1)."' ";
	mysqli_query($link, $query);
	$groupId = mysqli_insert_id($link);
}

if(mysqli_affected_rows($link)) {
	$success = true;
	$group = getGroup($groupId);
}

print json_encode(array(
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'group'=>$group,
	'success'=>$success,
	'groupId'=>$groupId,
	'query'=> $testMode ? $query : false
	), 
JSON_NUMERIC_CHECK);

?>