/* Update marital status */
UPDATE member SET married = '기혼' where spouseId > 0 or spouseName <> ''

CREATE TABLE IF NOT EXISTS `filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(31) NOT NULL,
  `q` varchar(127) NOT NULL,
  `filters` text NOT NULL,
  `createdAt` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


UPDATE member SET lanGroup='KM' where jundoId > 0;

/* UPDATE AND SET JUNDO ID FOR KM MEMBERS */
ALTER TABLE `groups` ADD `jageStart` varchar(10) NOT NULL COMMENT 'Jundo Year Range Start' AFTER `r`, ADD `jageEnd` varchar(10) NOT NULL COMMENT 'Jundo Year Range End' AFTER `jageStart`;
ALTER TABLE `groups` ADD `jGender` CHAR(1) NOT NULL AFTER `jageEnd`;

UPDATE groups SET jageStart='0', jageEnd='1937-12', jGender='M' where name = '무드셀라';
UPDATE groups SET jageStart='1938-01', jageEnd='1947-12', jGender='M' where name = '에녹';
UPDATE groups SET jageStart='1948-01', jageEnd='1950-12', jGender='M' where name = '아브라함';
UPDATE groups SET jageStart='1951-01', jageEnd='1953-12', jGender='M' where name = '이삭';
UPDATE groups SET jageStart='1954-01', jageEnd='1955-12', jGender='M' where name = '요셉';
UPDATE groups SET jageStart='1956-01', jageEnd='1958-12', jGender='M' where name = '모세';
UPDATE groups SET jageStart='1959-01', jageEnd='1960-12', jGender='M' where name = '여호수아';
UPDATE groups SET jageStart='1961-01', jageEnd='1963-12', jGender='M' where name = '갈렙';
UPDATE groups SET jageStart='1964-01', jageEnd='1968-12', jGender='M' where name = '보아스';
UPDATE groups SET jageStart='1969-01', jageEnd='1970-12', jGender='M' where name = '엘리야';
UPDATE groups SET jageStart='1971-01', jageEnd='1972-12', jGender='M' where name = '베드로';
UPDATE groups SET jageStart='1973-01', jageEnd='1974-12', jGender='M' where name = '안드레';
UPDATE groups SET jageStart='1975-01', jageEnd='1979-12', jGender='M' where name = '바울';
UPDATE groups SET jageStart='1980-01', jageEnd='1982-12', jGender='M' where name = '디모데';
UPDATE groups SET jageStart='1983-01', jageEnd='1995-12', jGender='M' where name = '빌립';
UPDATE groups SET jageStart='0', jageEnd='1937-12', jGender='F' where name = '미리암';
UPDATE groups SET jageStart='1938-01', jageEnd='1942-12', jGender='F' where name = '안나';
UPDATE groups SET jageStart='1943-01', jageEnd='1949-12', jGender='F' where name = '사라';
UPDATE groups SET jageStart='1950-01', jageEnd='1952-12', jGender='F' where name = '리브가';
UPDATE groups SET jageStart='1953-01', jageEnd='1955-12', jGender='F' where name = '라헬';
UPDATE groups SET jageStart='1956-01', jageEnd='1957-12', jGender='F' where name = '요게벳';
UPDATE groups SET jageStart='1958-01', jageEnd='1960-12', jGender='F' where name = '라합';
UPDATE groups SET jageStart='1961-01', jageEnd='1961-12', jGender='F' where name = '드보라';
UPDATE groups SET jageStart='1962-01', jageEnd='1963-12', jGender='F' where name = '나오미';
UPDATE groups SET jageStart='1964-01', jageEnd='1966-12', jGender='F' where name = '룻';
UPDATE groups SET jageStart='1967-01', jageEnd='1969-12', jGender='F' where name = '한나';
UPDATE groups SET jageStart='1970-01', jageEnd='1971-12', jGender='F' where name = '아비가일';
UPDATE groups SET jageStart='1972-01', jageEnd='1973-06', jGender='F' where name = '에스더';
UPDATE groups SET jageStart='1973-07', jageEnd='1974-12', jGender='F' where name = '다비다';
UPDATE groups SET jageStart='1975-01', jageEnd='1976-12', jGender='F' where name = '루디아';
UPDATE groups SET jageStart='1977-01', jageEnd='1978-12', jGender='F' where name = '뵈뵈';
UPDATE groups SET jageStart='1979-01', jageEnd='1982-12', jGender='F' where name = '브리스길라';
UPDATE groups SET jageStart='1983-01', jageEnd='1995-12', jGender='F' where name = '유니게';

SELECT m1.mid, m1.kname, m1.birthDate, m1.adultLevel, m1.jundoId, g1.groupId, g1.name as groupName from member as m1
LEFT JOIN groups as g1 on 
	g1.groupType = 3 and g1.jGender = m1.gender and
	substr(m1.birthDate, 1, 7)>= g1.jageStart and substr(m1.birthDate, 1, 7)<=g1.jageEnd
WHERE m1.lanGroup = 'KM' and m1.birthDate <> '' and m1.jundoLeader = 0 
	and TIMESTAMPDIFF(YEAR, birthDate, CURDATE()) > 20 and (adultLevel <> '청년' && adultLevel <> '학생')
ORDER BY jundoId, birthDate

UPDATE member as m1
LEFT JOIN groups as g1 on 
	g1.groupType = 3 and g1.jGender = m1.gender and
	substr(m1.birthDate, 1, 7)>= g1.jageStart and substr(m1.birthDate, 1, 7)<=g1.jageEnd
SET m1.jundoId = g1.groupId
WHERE m1.lanGroup = 'KM' and m1.birthDate <> '' and m1.jundoLeader = 0 
	and TIMESTAMPDIFF(YEAR, birthDate, CURDATE()) > 20 and (adultLevel <> '청년' && adultLevel <> '학생')

UPDATE `member` SET `gender` = 'M' WHERE `member`.`mid` = 261;
UPDATE `member` SET `gender` = 'M' WHERE `member`.`mid` = 397;
UPDATE `member` SET `gender` = 'F' WHERE `member`.`mid` = 1301;



/* DB STRUCTURE CHANGE */
CREATE TABLE IF NOT EXISTS `group_map` (
  `mid` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `leader` int(11) NOT NULL,
  PRIMARY KEY (`mid`,`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* RUN update_groupmap.php */
ALTER TABLE `member` DROP `cellId`, DROP `jundoId`, DROP `jejikId`, DROP `dept1`, DROP `dept2`, 
DROP `dept3`, DROP `dept4`, DROP `cellLeader`, DROP `jejikLeader`, DROP `jundoLeader`, 
DROP `dept1Leader`, DROP `dept2Leader`, DROP `dept3Leader`, DROP `dept4Leader`;


/* auth token */
ALTER TABLE `admin` ADD `authtoken` VARCHAR(20) NOT NULL AFTER `level`;



/* CREATE CHURCH */
CREATE TABLE IF NOT EXISTS `church` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `ename` varchar(128) NOT NULL,
  `addressId` int(11) NOT NULL,
  `formatted` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `config` text NOT NULL,
  `createdAt` int(11) NOT NULL,
  `updatedAt` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `church` (`id`, `name`, `ename`, `addressId`, `formatted`, `level`, `config`, `createdAt`, `updatedAt`, `deleted`) VALUES
(1, '필라안디옥교회', 'Antioch Church of Philadelphia', 6, '1 Antioch Ave, Conshohocken, PA 19428', 1, '', 0, 0, 0);

ALTER TABLE `admin` ADD `churchId` INT NOT NULL DEFAULT '1' AFTER `adminId`;
ALTER TABLE `filters` ADD `churchId` INT NOT NULL DEFAULT '1' AFTER `id`;
ALTER TABLE `groups` ADD `churchId` INT NOT NULL DEFAULT '1' AFTER `groupId`;
ALTER TABLE `member` ADD `churchId` INT NOT NULL DEFAULT '1' AFTER `mid`;
ALTER TABLE `training` ADD `churchId` INT NOT NULL DEFAULT '1' AFTER `id`;
ALTER TABLE `visit` ADD `churchId` INT NOT NULL DEFAULT '1' AFTER `vid`;

ALTER TABLE `admin` DROP INDEX `email`, ADD UNIQUE `email` (`email`, `churchId`) USING BTREE;


/* NEW COMERS */ 
INSERT INTO `groups` (`groupId`, `churchId`, `name`, `groupType`, `l`, `r`, `jageStart`, `jageEnd`, `jGender`, `deleted`) VALUES
(277, 1, '새가족', 6, 2, 3, '', '', '', 0),
(6, 1, '새가족', 6, 1, 4, '', '', '', 0);
/* RUN newcomer_update.php */

/* GROUP ADMIN */
ALTER TABLE `admin` ADD `groupId` INT NOT NULL AFTER `level`;

/* HISTORY */
CREATE TABLE IF NOT EXISTS `history_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `query` text NOT NULL,
  `operation` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `history_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `query` text NOT NULL,
  `operation` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `history_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `query` text NOT NULL,
  `field` varchar(31) NOT NULL,
  `operation` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `history_training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classId` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `query` text NOT NULL,
  `operation` varchar(31) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `history_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `query` text NOT NULL,
  `operation` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* LOGIN HISTORY */
CREATE TABLE IF NOT EXISTS  `history_login` ( 
  `id` INT NOT NULL AUTO_INCREMENT , 
  `adminId` INT NOT NULL , 
  `timestamp` INT NOT NULL , 
  `userAgent` VARCHAR(511) NOT NULL , 
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
/* CARD HISTORY */
CREATE TABLE IF NOT EXISTS `history_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


