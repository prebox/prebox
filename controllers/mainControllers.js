'use strict';

angular.module('prebox.mainControllers', [])

/*********** AGENT ABSTRACT ***********/
.controller('AppCtrl', function($scope, $state, $rootScope, gService) {
    if(!live) console.log("Header app");
    $scope.group = gCurrent.group;
    $scope.mygroup = gCurrent.mygroup;
    $scope.groups = gCurrent.groups;
    $scope.showMessage = function() {
      $rootScope.message('message', 'title', 'content', function() {
      });
    }
    $scope.navChange = function(obj, param) {
        $('.sidebar-collapse').addClass('collapse').removeClass('in');
        /*if(obj.nav == 'home') {
          $('.back-button').hide();
        } else 
          $('.back-button').show();
        */
        $scope.current = obj;
        if(param)
          $scope.current.param = param;
    }
    $scope.toggleGroups = function(collapse) {
      if(!$scope.groups.length) return;
      if($('li.company').hasClass('expand') || collapse) {
        $('li.company').removeClass('expand');
        $('.expand-content').css('height', 0);
      } else {
        $('li.company').addClass('expand');
        var height = ($scope.groups.length + 1) * 1.4 + 1;
        $('.expand-content').css('height', height + 'em');
      }
    }
    $scope.changeGroup = function(groupId) {
      if(groupId == $scope.mygroup.groupId)
        $scope.group = angular.copy($scope.mygroup);
      else
        $scope.group = gService.getGroup(groupId);
      gCurrent.group = $scope.group;
      $scope.toggleGroups(true);
      $state.reload();
    }
    $scope.logout = function() {
      $rootScope.message('confirm', $rootScope.local('msg.logout_confirm', 'en'), $rootScope.local('msg.logout_confirm2', 'en'), function() {
        $rootScope.showLoading($rootScope.local('msg.logout_loader', 'en'));
        setTimeout(function() {
          $rootScope.hideLoading();
          gSetting = {};
          gService.removeCookie('adminId');
          gService.removeCookie('level');
          localStorage.removeItem('level');
          $state.go('login');
        }, 500);
        
      })
    }
})

/*********** RETAILER ABSTRACT ***********/
.controller('RetailerCtrl', function($scope, $state, $rootScope, gService) {
    if(!live) console.log("Retailer Header app");
    $scope.group = gCurrent.group;

    $scope.showMessage = function() {
      $rootScope.message('message', 'title', 'content', function() {
      });
    }
    $scope.navChange = function(obj, param) {
        $('.sidebar-collapse').addClass('collapse').removeClass('in');

        $scope.current = obj;
        if(param)
          $scope.current.param = param;
    }

    $scope.logout = function() {
      $rootScope.message('confirm', $rootScope.local('msg.logout_confirm', 'en'), $rootScope.local('msg.logout_confirm2', 'en'), function() {
        $rootScope.showLoading($rootScope.local('msg.logout_loader', 'en'));
        setTimeout(function() {
          $rootScope.hideLoading();
          gSetting = {};
          gService.removeCookie('adminId');
          gService.removeCookie('level');
          localStorage.removeItem('level');
          $state.go('login');
        }, 500);
        
      })
    }
})

.controller('LoginCtrl', function($scope, $state, $rootScope, webService, gService) {
    var username = localStorage.getItem('username');
    var security_code = localStorage.getItem('security_code');
    if(!username || username == 'undefined') username = "";
    $('body').removeClass('retailer');
    $scope.login = {
      'username': username,
      'security_code': security_code,
      'remember': true
    }

    $('input.email').blur(function() {
      $scope.remember();  
    });
    $scope.remember = function() {
      if($scope.login.remember) {
        localStorage.setItem('username', $scope.login.username);
        localStorage.setItem('security_code', $scope.login.security_code);
      } else {
        localStorage.setItem('username', "");
      }
    }

    $scope.submit = function() {

      $rootScope.showLoading($rootScope.local('msg.login_loader', 'en'));
        webService.postData(gConfig.apiBase + 'post/login', $scope.login, true).then(function(result) {
          $rootScope.hideLoading();
          if(result.success) {
            gService.setCookie('userId', result.user.userId, 365);
            gService.setCookie('level', result.user.level, 365);
            localStorage.setItem('authtoken', result.random);
            localStorage.setItem('deviceId', result.deviceId);
            $rootScope.loadUser(result);
            if(result.groupType == 'AGENT')
              $state.go('app.home');
            else
              $state.go('retailer.rhome');
          }
          else {
            $rootScope.message('message', $rootScope.local('msg.login_fail', 'en'), $rootScope.local('msg.login_fail2', 'en'));
          }
        })
    }
})

.controller('ForgotCtrl', function($scope, $state, $rootScope, webService) {
  var email = localStorage.getItem('email');
  if(!email || email == 'undefined') email = "";
  $scope.login = { email: email };
  $scope.step = 1;
  $scope.submit1 = function() {
    webService.postData(gConfig.apiBase + 'post/forgot1', $scope.login).then(function(result) {
      if(result.success) {
        $scope.step = 2;
        $scope.adminId = result.adminId;
        $scope.code = result.code;
      } else {
        $rootScope.message('message', $rootScope.local('forgot.fail_title', 'en'), result.errMsg);
      }
    });
  }
  $scope.submit2 = function() {
    if($scope.code == $scope.login.code) {
      $scope.step = 3;
      $scope.login = {
        adminId2: $scope.adminId
      };
    } else {
      $rootScope.message('message', $rootScope.local('forgot.fail_title', 'en'), "Invalid code entered. Please check your email for correct code.");
    }
  }
  $scope.submit3 = function() {
    if($scope.login.password != $scope.login.password2) {
      $rootScope.message('message', $rootScope.local('forgot.fail_title', 'en'), "Confirmed password must match the password you enetered");
    } 
    webService.postData(gConfig.apiBase + 'post/forgot3', $scope.login).then(function(result) {
      if(result.success) {
        $scope.step = 4;
      }
    });
  }
})


.controller('aTestCtrl', function($scope, $state, $rootScope) {
    $scope.header = $state.current.header;
    $scope.$parent.navChange($scope.header);
})