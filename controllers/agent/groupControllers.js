'use strict';

angular.module('prebox.groupControllers', [])

.controller('aHomeCtrl', function($scope, $state, $rootScope) {
    $scope.header = $state.current.header;
    $scope.$parent.navChange($scope.header);
    $scope.cities = [];
    $scope.myform = {

    }

})

.controller('aGroupsCtrl', function($scope, $state, $rootScope, webService, gService) {
    $scope.header = $state.current.header;
    $scope.$parent.navChange($scope.header);
    $scope.id = +($state.params.id);
    $scope.id = gCurrent.group.groupId;

    $scope.init = function() {
    	$scope.page = {
            size: 100,
            total: null,
            current: 1
      }
      $scope.membersSelected = {};
      $scope.sortOrder = 0;
      $scope.getData(1);
    }

    /* GET training data */
    $scope.getData = function(newPage, exportFlag) {
        // Keyword Search
        /*
        if(exportFlag) {
          var filePath = gConfig.apiBase + '/group/getGroups.php?authtoken=' + localStorage.getItem('authtoken') + '&groupId=' + $scope.id +'&exportFlag=1';
          console.log(filePath);
          window.location = filePath;
          return ;
        }
        */
        webService.postData(gConfig.apiBase + 'post/groups', 
            { groupId: $scope.id, 
              psize: $scope.page.size, pcurrent: newPage, sortKey: $scope.sortKey, sortOrder: $scope.sortOrder }).then(function(result) {
            $scope.newResult(result); 
        });
    }
    $scope.newResult = function(result) {
      $scope.page.total = result.total;
      $scope.groups = result.groups;
    }
    $scope.sort = function(id) {
      if($scope.sortKey == id) {
        // Reverse sortorder
        $scope.sortOrder = 1 - $scope.sortOrder;
      } else {
        $scope.sortKey = id;
        $scope.sortOrder = 0;
      }
      $scope.getData(1);
    }
    $scope.exportResult = function() {
      $scope.getData(1, 1);
    }
    $scope.refreshGroup = function() {
      $scope.getData(1);
      // Refresh groups in left column
      webService.getData(gConfig.apiBase + 'get/init').then(function(result) {
          $rootScope.loadUser(result);
          $scope.$parent.groups = gCurrent.groups;
      });
    }

    /* REMOVING FROM GROUP */
    $scope.selectAll = function() {
      $scope.membersSelectAll = !$scope.membersSelectAll;
      if($scope.membersSelectAll ) {
        $( "input.member" ).prop('checked', true);
        $( "input.member" ).each(function( index ) {
          $scope.membersSelected[index] = +($(this).attr('ng-true-value'));
        });
      } else {
        $( "input.member" ).prop('checked', false);
        $scope.membersSelected = {};
      }
    }
    $scope.selectMember = function(index) {
      if(!$scope.membersSelected[index]) {
        delete $scope.membersSelected[index];
      }
    }

    // Add New Account popup
    $scope.addNewAccount = function() {
      $scope.newacct =  {
        groupType: 'DEALER'
      }
      $rootScope.toggleModal('addnew');
    }
    $scope.editGroup = function(groupId) {
      //$scope.$parent.group = groupId;
      gCurrent.groupId = groupId;
      gCurrent.group = gService.getGroup(groupId);
      $state.go('app.account.general');
    }
    $scope.deleteMember = function() {
      var id = JSON.stringify($scope.membersSelected);
      $rootScope.delete('groups', id, function() {
        $scope.refreshGroup();
      });
    }


    $scope.submitAdd = function() {
      $scope.newacct.parentId = gCurrent.group.groupId;
      webService.postData(gConfig.apiBase + 'post/general', $scope.newacct ).then(function(result) {
          $rootScope.modalShown = false;
          if(result.success) {
            $scope.general = angular.copy(result.group);
            $rootScope.message('message', "Added successfully!", "", function() {
              $scope.refreshGroup();
            });
          }
          else {
            $rootScope.message('message', "Add failed", "");
          }
      });
    }

    $scope.init();
})


