<?php

// Get DB info by query; (First row)
function getdbquery($query)
{
	global $link;
	$resulttemp = mysqli_query($link, $query);
	if(!$resulttemp) return false;
	$temp = mysqli_fetch_assoc($resulttemp);
	if(is_array($temp)) {
		$myarray = array();
		foreach($temp as $key => $value) $myarray[$key] = $value;
		return $myarray;
	}
	else return false;
}
// Get DB info by query; (Multiple Rows with limit of 1000 rows)
function getdbrows($query)
{
	global $link;
	$myarray = array();
	$resulttemp = @mysqli_query($link, $query);
	$count = 0;
	while($row = @mysqli_fetch_assoc($resulttemp))
	{
		foreach ($row as $key => $value) 
			$myarray[$count][$key] = $value;
		$count++;
		if($count>=10000) break;
	}
	if(count($myarray)) return $myarray;
	else return [];
}

function logAdmin($operation, $adminId2, $query) {
	global $adminId, $link, $now;
	$q = "INSERT INTO history_admin SET aid='$adminId2', adminId='$adminId', timestamp='$now', operation='$operation', query='".addslashes($query)."' ";
	mysqli_query($link, $q);
}
function logMember($operation, $mid, $query, $field="") {
	global $adminId, $link, $now;
	$q = "INSERT INTO history_member SET mid='$mid', adminId='$adminId', timestamp='$now', field='$field', operation='$operation', query='".addslashes($query)."' ";
	mysqli_query($link, $q);
}
function logGroup($operation, $groupId, $query) {
	global $adminId, $link, $now;
	$q = "INSERT INTO history_group SET groupId='$groupId', adminId='$adminId', timestamp='$now', operation='$operation', query='".addslashes($query)."' ";
	mysqli_query($link, $q);
}
function logVisit($operation, $vid, $query) {
	global $adminId, $link, $now;
	$q = "INSERT INTO history_visit SET vid='$vid', adminId='$adminId', timestamp='$now', operation='$operation', query='".addslashes($query)."' ";
	mysqli_query($link, $q);
}
function logTraining($operation, $classId, $query, $mid=0) {
	global $adminId, $link, $now;
	$q = "INSERT INTO history_training SET classId='$classId', mid='$mid', adminId='$adminId', timestamp='$now', operation='$operation', query='".addslashes($query)."' ";
	mysqli_query($link, $q);
}
function logLogin() {
	global $userId, $link, $now;
	$q = "INSERT INTO history_login SET userId='$userId', timestamp='$now', userAgent='".addslashes($_SERVER['HTTP_USER_AGENT'])."' ";
	mysqli_query($link, $q);
}
function logCard($mid) {
	global $adminId, $link, $now;
	$q = "INSERT INTO history_card SET adminId='$adminId', timestamp='$now', mid='$mid' ";
	mysqli_query($link, $q);
}

function jsonParse($str) {
	if(!trim($str))
		return false;
	else
		return json_decode($str, true);
}

function getAdmin($id) {
	return getdbquery("SELECT *, DATE_FORMAT(FROM_UNIXTIME(createdAt), '%Y-%m-%d') as dateCreated from admin where adminId='$id'");
}

function getSetting($id) {
	$admin = getdbquery("SELECT * from admin where adminId='$id'");
	if($admin) {
		$result = json_decode($admin['setting'], true);
		return $result;
	}
	else
		return false;
}

function randomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
}

function make_thumb($src, $dest, $desired_width) {

	/* read the source image */
	$source_image = imagecreatefromjpeg($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}

function send_email($fromemail, $emailto, $subject, $bodytext, $fromname)
{
	global $rootPath, $link;
	

	if(!$fromname) $fromname = $fromemail;
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	// Additional headers
	$headers .= "From: $fromname <$fromemail>" . "\r\n";
	$headers .= "Reply-To: $fromname <$fromemail>" . "\r\n";

	$body = $bodytext;
	$body = str_replace("\n", "<br>", $body);
	mail($emailto, $subject, $body, $headers);
	
/*
	require_once($rootPath.'/phpmailer/class.phpmailer.php');
	require_once($rootPath."/phpmailer/class.smtp.php"); // 

	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSMTP(); // telling the class to use SMTP

	try {
	  $mail->Host = 'smtp.gmail.com';              // Specify main and backup SMTP servers
	  $mail->SMTPAuth = true;                               // Enable SMTP authentication
	  $mail->Username = "pnf1214@gmail.com";
	  $mail->Password = "faith12141";                       // SMTP password
	  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	  $mail->Port = 587;    

	  $mail->AddAddress($emailto);
	  $mail->SetFrom($fromemail, $fromname);
	  $mail->AddReplyTo($fromemail, $fromname);
	  $mail->IsHTML(true);
	  $mail->Subject = $subject;
	  $mail->Body    = $bodytext;

	  $mail->Send();
	} catch (phpmailerException $e) {
	  echo $e->errorMessage(); //Pretty error messages from PHPMailer
	} catch (Exception $e) {
	  echo $e->getMessage(); //Boring error messages from anything else!
	}
*/
}

?>