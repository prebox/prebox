<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/addressFunctions.php");
include_once(dirname(__FILE__)."/../_lib/userFunctions.php");

$setStr = "
		groupId = '$groupId',
		name = '".addslashes($name)."',
		username = '".addslashes($username)."',
		password = '".addslashes($password)."',
		security_code = '$security_code',
		email = '".addslashes($email)."',
		level = '1'
";
$success = false;

// EDIT
if($userId) {
	$queryMain = "UPDATE user SET $setStr, updatedAt ='$now' WHERE userId = '$userId'";
	mysqli_query($link, $queryMain);
} 

// ADD
else  {
	$queryMain = "INSERT INTO user SET $setStr, createdAt = '$now' ";
	mysqli_query($link, $queryMain);
	$userId = mysqli_insert_id($link);
}

if(mysqli_affected_rows($link)) {
	$success = true;
	$user = getUser($userId);
}

print json_encode(array(
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'group'=>$group,
	'success'=>$success,
	'groupId'=>$groupId,
	'query'=> $testMode ? $queryMain : false
	), 
JSON_NUMERIC_CHECK);

?>