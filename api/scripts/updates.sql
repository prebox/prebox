

/* DEVICES */
CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `deviceId` varchar(128) NOT NULL,
  `authtoken` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `user_agent` varchar(511) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `lastactivity` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adminId` (`adminId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* auth token */
ALTER TABLE `admin` DROP `authtoken`;