<?php

$query = "
CREATE TABLE `prebox`.`group_direct_cash` ( 
	`id` INT NOT NULL AUTO_INCREMENT , 
	`fromId` INT NOT NULL , `toId` INT NOT NULL , 
	`amount` DECIMAL(10,2) NOT NULL , 
	`method` VARCHAR(15) NOT NULL DEFAULT 
	'CASH' , `description` VARCHAR(255) NOT NULL , 
	`txNumber` VARCHAR(31) NOT NULL , 
	`createdAt` INT NOT NULL , 
	`deleted` BOOLEAN NOT NULL , 
	PRIMARY KEY (`id`)
) ENGINE = InnoDB;
";
	
?>
