'use strict';

var gInit = false;
var gCurrent = {}
var gSetting = false;
var host = window.location.hostname;
var live = host.indexOf('localhost') < 0;

var gConfig = {
    "isLive": false,
	"htmlBase": 'views/',
	"map": {
		"googleKeyServer": 'AIzaSyA8l29Soj99o4yddmEaz4hoOQ74EtMYtic',
        "googleKey": 'AIzaSyAmpvjSE4u8ENxMTZaYfT2aB3UTcjSiAEc',
        "googleKeyAndroid": 'AIzaSyAOZ7V6THQSgeM1ZK42uWhhw6O0u8vwzvk',
        "googleKeyIos": 'AIzaSyAI_IwC1Do_bLy-mvLSpYbUPHrcEPiC2cQ',
        "geoCodeUrl": 'https://maps.googleapis.com/maps/api/geocode/json?',
        "distanceUrl": 'https://maps.googleapis.com/maps/api/distancematrix/json?'
    },
    "importTime": 200,
    "apiBase": 'http://' + host + '/prebox/API/',
    "apiBase": live ? '/API/' : '/prebox/API/',
    "imageBase": 'http://' + host + '/prebox/img/',
    "addressBase": 'https://maps.googleapis.com/maps/api/geocode/json?address='
}

var gPlaceholder = {
    importSample: 'Example:\n89014102255981131495_718776495\n89014102256169421401_755542001\n89014102256169421500_755543000\n89014102256169421591_755543991'
}
