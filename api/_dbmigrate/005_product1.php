<?php

$query = "
CREATE TABLE `prebox`.`product` ( 
	`id` INT NOT NULL AUTO_INCREMENT , 
	`typeId` INT NOT NULL ,
	`productCode` VARCHAR(63) NOT NULL , 
	`name` VARCHAR(63) NOT NULL , 
	`action` VARCHAR(15) NOT NULL DEFAULT 'PIN' , 
	`createdAt` INT NOT NULL , 
	`updatedAt` INT NOT NULL , 
	`deleted` BOOLEAN NOT NULL , 
	PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

";

?>
