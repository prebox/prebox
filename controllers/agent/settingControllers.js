'use strict';

angular.module('prebox.settingControllers', [])

.controller('aSettingSimCtrl', function($scope, $state, $rootScope, webService, gService) {
    $scope.header = $state.current.header;
    $scope.$parent.navChange($scope.header);
    $scope.groupId = gCurrent.group ? gCurrent.group.groupId : 0;

    $scope.init = function() {
        $scope.page = {
            size: 100,
            total: null,
            current: 1
      }
      $scope.form = {};
      $scope.membersSelected = {};
      $scope.placeholder = gPlaceholder.importSample;
      $scope.sortOrder = 0;
      $scope.getData(1);
    }

    /* GET training data */
    $scope.getData = function(newPage, exportFlag) {
        /*
        if(exportFlag) {
          var filePath = gConfig.apiBase + '/account/getUsers.php?authtoken=' + localStorage.getItem('authtoken') + '&groupId=' + $scope.groupId +'&exportFlag=1';
          console.log(filePath);
          window.location = filePath;
          return ;
        }
        */
        webService.postData(gConfig.apiBase + 'post/sims', 
            { groupId: $scope.groupId, 
              psize: $scope.page.size, pcurrent: newPage, sortKey: $scope.sortKey, sortOrder: $scope.sortOrder }).then(function(result) {
            $scope.newResult(result); 
        });
    }
    $scope.newResult = function(result) {
      $scope.page.total = result.total;
      $scope.sims = result.sims;
    }
    $scope.sort = function(id) {
      if($scope.sortKey == id) {
        // Reverse sortorder
        $scope.sortOrder = 1 - $scope.sortOrder;
      } else {
        $scope.sortKey = id;
        $scope.sortOrder = 0;
      }
      $scope.getData(1);
    }
    $scope.exportResult = function() {
      $scope.getData(1, 1);
    }

    /* REMOVING FROM GROUP */
    $scope.selectAll = function() {
      $scope.membersSelectAll = !$scope.membersSelectAll;
      if($scope.membersSelectAll ) {
        $( "input.member" ).prop('checked', true);
        $( "input.member" ).each(function( index ) {
          $scope.membersSelected[index] = +($(this).attr('ng-true-value'));
        });
      } else {
        $( "input.member" ).prop('checked', false);
        $scope.membersSelected = {};
      }
    }
    $scope.selectMember = function(index) {
      if(!$scope.membersSelected[index]) {
        delete $scope.membersSelected[index];
      }
    }

    // Add New Account popup
    $scope.addNewSims = function() {
      $rootScope.toggleModal('addsim');
      setTimeout(function() { $('.import textarea').focus(); }, 0);
    }
    $scope.deleteMember = function() {
      var id = JSON.stringify($scope.membersSelected);
      $rootScope.delete('sim', id, function() {
        $scope.getData(1);
      });
    }
    $scope.editSim = function() {
      console.log("EDIT");
    }

    /* CHECK SIMS */
    $scope.checkSims = function() {
      var list = $scope.form.import;
      if(list.length) {
        webService.postData(gConfig.apiBase + 'post/simcheck', {content: list} ).then(function(result) {
          $scope.form.available = result.available.join("\n");
          $scope.form.existing = result.existing.join("\n");
        });
      }
    }

    $scope.saveSims = function() {
        var list = $scope.form.available;
        webService.postData(gConfig.apiBase + 'post/simsave', {content: list, groupId: $scope.groupId} ).then(function(result) {
          $rootScope.modalShown = false;
          $scope.form = {};
          if(result.success) {
            $rootScope.message('message', "Saved successfully!", "");
            $scope.getData(1);
          }
          else {
            $rootScope.message('message', "Save failed", "");
          }
        });
    }
    $scope.init();

})

