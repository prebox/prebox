<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/userFunctions.php");

$user = getUser($userId);

print json_encode(array(
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'user'=>$user,
	'query'=> $testMode ? $queryMain : false
	), 
JSON_NUMERIC_CHECK);

?>