angular.module('services.import', [])

.service('importService', function($q, $http, $rootScope, webService) {
  return {

    checkCol: function(haystack, needle) {
      for(var i=0; i<haystack.length; i++) {
        if(haystack[i].kid == needle)
          return i+1;
      }
      return 0;
    },
    
    verifyImportText: function(type, line) {
      if(type=='training') return true;
      var cols = line.split('\t');
      var goods = 0;
      var bads = 0;
      var colmap = type=='member' ? gColmap : gColmap2;
      for(var i=0; i<cols.length; i++) {
        var order = this.checkCol(colmap, cols[i]);
        if(order) {
          cols[i] = order;
          goods++;
        }
        else
          bads++;
      }
      if(!live) console.log("GOODS " + goods, "BADS " + bads);
      if( (type=='member' && goods >= 3) || (type=='visit' && goods >= 3) ) {
        gCurrent.colOrder = cols;
        if(!live) console.log(gCurrent.colOrder);
        return true;
      }
      else
        return false;
    },

    verifyImportFile: function(type, data) {
      var goods = 0;
      var bads =0;
      var colmap = type=='member' ? gColmap : gColmap2;
      var i = 0;
      var cols = [];
      for(var key in data) {
        var order = this.checkCol(colmap, key);
        if(order) {
          cols.push(order);
          goods++;
        } else 
          bads++;
        i++;
      }
      if(!live) console.log("GOODS " + goods, "BADS " + bads);
      if( (type=='member' && goods >= 5) || (type=='visit' && goods >= 5) ) {
        gCurrent.colOrder = cols;
        if(!live) console.log(gCurrent.colOrder);
        return true;
      }
      else
        return false;
    },

    /************** IMPORT Member OPERATION (TEXT) ***********/
    insertTraining: function(type, line) {
      var cols = line.split('\t');
      var qstring = "";
      if(cols.length >= 2) {
        var params = {};
        if(cols[1]) { // year semester
          params.year = cols[1].substring(0,4);
          var semester = cols[1].substring(cols[1].length - 1);
          if(semester == '상') params.semester = 1;
          else if(semester == '하') params.semester = 2;
        }
        if(cols[0]) {
          params.name = cols[0];
        }
        params.type = type;
        if(params.name && params.year > '2000') {
          webService.postData(gConfig.apiBase + "post/import/training", params, true).then(function(result) {
            if(!live) console.log(result.success);
          });
        }
      }
    },

    /************** IMPORT Member OPERATION (TEXT) ***********/
    insertMemberCsv: function(line) {
      var cols = line.split('\t');
      var qstring = "";
      if(cols.length >= 3) {
        var params = {}
        if(gCurrent.colOrder.indexOf(1) >=0) 
          params.mid = cols[gCurrent.colOrder.indexOf(1)];
        if(gCurrent.colOrder.indexOf(14) >=0 || gCurrent.colOrder.indexOf(15) >=0) 
          params.street = cols[gCurrent.colOrder.indexOf(15)];
        if(gCurrent.colOrder.indexOf(13) >=0) 
          params.city = cols[gCurrent.colOrder.indexOf(13)];
        if(gCurrent.colOrder.indexOf(11) >=0 || gCurrent.colOrder.indexOf(12) >=0) 
          params.state = cols[gCurrent.colOrder.indexOf(12)];
        if(gCurrent.colOrder.indexOf(16) >=0 || gCurrent.colOrder.indexOf(17) >=0) 
          params.zip = cols[gCurrent.colOrder.indexOf(16)];
        if(gCurrent.colOrder.indexOf(21) >=0) params.cellGroup = cols[gCurrent.colOrder.indexOf(21)];
        if(gCurrent.colOrder.indexOf(22) >=0) params.cell = cols[gCurrent.colOrder.indexOf(22)];
        if(gCurrent.colOrder.indexOf(23) >=0) params.cellLeader = cols[gCurrent.colOrder.indexOf(23)];
        if(gCurrent.colOrder.indexOf(24) >=0) params.jundo = cols[gCurrent.colOrder.indexOf(24)];
        if(gCurrent.colOrder.indexOf(25) >=0) params.jundoLeader = cols[gCurrent.colOrder.indexOf(25)];
        if(gCurrent.colOrder.indexOf(26) >=0 || gCurrent.colOrder.indexOf(50) >=0) {
          params.jejik = cols[gCurrent.colOrder.indexOf(50)];
        }
        if(gCurrent.colOrder.indexOf(28) >=0) params.dept1 = cols[gCurrent.colOrder.indexOf(28)];
        if(gCurrent.colOrder.indexOf(29) >=0) params.dept1Leader = cols[gCurrent.colOrder.indexOf(29)];
        if(gCurrent.colOrder.indexOf(30) >=0) params.dept2 = cols[gCurrent.colOrder.indexOf(30)];
        if(gCurrent.colOrder.indexOf(31) >=0) params.dept2Leader = cols[gCurrent.colOrder.indexOf(31)];
        if(gCurrent.colOrder.indexOf(32) >=0) params.dept3 = cols[gCurrent.colOrder.indexOf(32)];
        if(gCurrent.colOrder.indexOf(33) >=0) params.dept3Leader = cols[gCurrent.colOrder.indexOf(33)];
        if(gCurrent.colOrder.indexOf(34) >=0) params.dept4 = cols[gCurrent.colOrder.indexOf(34)];
        if(gCurrent.colOrder.indexOf(35) >=0) params.dept4Leader = cols[gCurrent.colOrder.indexOf(35)];
        if(gCurrent.colOrder.indexOf(47) >=0) params.jobAddress = cols[gCurrent.colOrder.indexOf(46)];

        if(!live) console.log(params);
        for(var i=0; i<cols.length; i++) {
          if(!gColmap[ gCurrent.colOrder[i]-1]) continue;
          if(gColmap[ gCurrent.colOrder[i]-1].skip) {
            continue;
          } else {
            qstring += this.processColumn(cols[i], angular.copy(gColmap[gCurrent.colOrder[i]-1]));
          }
        }
        if(qstring || params.mid) {
          params.qstring = qstring;
          webService.postData(gConfig.apiBase + "post/import/member", params, true).then(function(result) {
            if(!live) console.log(result.success);
          });
        }
      }
    },
    /************** IMPORT Member OPERATION (EXCEL) ***********/
    insertMemberExcel: function(data) {
      var qstring = "";
      var params = {};
      if(gCurrent.colOrder.indexOf(1) >=0) params.mid = data[ gColmap[0].kid ];
      if(gCurrent.colOrder.indexOf(14) >=0) params.street = data[ gColmap[13].kid ];
      if(gCurrent.colOrder.indexOf(15) >=0) params.street = data[ gColmap[14].kid ];
      if(gCurrent.colOrder.indexOf(13) >=0) params.city = data[ gColmap[12].kid ];
      if(gCurrent.colOrder.indexOf(11) >=0) params.state = data[ gColmap[10].kid ];
      if(gCurrent.colOrder.indexOf(12) >=0) params.state = data[ gColmap[11].kid ];
      if(gCurrent.colOrder.indexOf(16) >=0) params.zip = data[ gColmap[15].kid ];
      if(gCurrent.colOrder.indexOf(17) >=0) params.zip = data[ gColmap[16].kid ];
      if(gCurrent.colOrder.indexOf(21) >=0) params.cellGroup = data[ gColmap[20].kid ];
      if(gCurrent.colOrder.indexOf(22) >=0) params.cell = data[ gColmap[21].kid ];
      if(gCurrent.colOrder.indexOf(23) >=0) params.cellLeader = data[ gColmap[22].kid ];
      if(gCurrent.colOrder.indexOf(24) >=0) params.jundo = data[ gColmap[23].kid ];
      if(gCurrent.colOrder.indexOf(25) >=0) params.jundoLeader = data[ gColmap[24].kid ];
      if(gCurrent.colOrder.indexOf(26) >=0) params.jejik = data[ gColmap[25].kid ];
      if(gCurrent.colOrder.indexOf(50) >=0) params.jejik = data[ gColmap[49].kid ];
      if(gCurrent.colOrder.indexOf(28) >=0) params.dept1 = data[ gColmap[27].kid ];
      if(gCurrent.colOrder.indexOf(29) >=0) params.dept1Leader = data[ gColmap[28].kid ];
      if(gCurrent.colOrder.indexOf(30) >=0) params.dept2 = data[ gColmap[29].kid ];
      if(gCurrent.colOrder.indexOf(31) >=0) params.dept2Leader = data[ gColmap[30].kid ];
      if(gCurrent.colOrder.indexOf(32) >=0) params.dept3 = data[ gColmap[31].kid ];
      if(gCurrent.colOrder.indexOf(33) >=0) params.dept3Leader = data[ gColmap[32].kid ];
      if(gCurrent.colOrder.indexOf(34) >=0) params.dept4 = data[ gColmap[33].kid ];
      if(gCurrent.colOrder.indexOf(35) >=0) params.dept4Leader = data[ gColmap[34].kid ];
      if(gCurrent.colOrder.indexOf(47) >=0) params.jobAddress = data[ gColmap[46].kid ];
      var i=0;
      for(var key in data) {
        if(gColmap[ gCurrent.colOrder[i]-1 ] && !gColmap[ gCurrent.colOrder[i]-1 ].skip) {
          qstring += this.processColumn(data[key], angular.copy(gColmap[gCurrent.colOrder[i]-1]) );
          i++;
        } else {
          i++;
          continue; 
        }
      }
      if(qstring || params.mid) {
        params.qstring = qstring;
        webService.postData(gConfig.apiBase + "post/import/member", params, true).then(function(result) {
          if(!live) console.log(result.success);
        });
      }
    },
    processName: function(name) {
      if(name.match(/[a-z]/i)) {
        return {
          first: name.split(' ').slice(0, -1).join(' '),
          last: name.split(' ').slice(-1).join(' '),
          english: true
        }
      } else
      return {
        name: name,
        english: false
      };
    },
    processColumn: function(value, hinfo) {
      if(hinfo.special) {
        var q = "";
        switch(hinfo.cid) {
          case 'name':
            var name2 = false;
            var matches = value.match(/\((.*?)\)/);
            var nameObj = {}, nameObj2 = {}
            if(matches) { // has secondary name
              nameObj = this.processName( (value.substr(0, value.indexOf('('))).trim() );
              nameObj2 = this.processName(matches[1]);
              if(nameObj2.english)
                q += " firstName='" + nameObj2.first.replace(/'/g, "\\'") + "', lastName='" + nameObj2.last.replace(/'/g, "\\'") + "', ";
              else
                q += " kname='" + nameObj2.name + "', ";
            } else {
              nameObj = this.processName(value);
            }
            if(nameObj.english) {
              var mainName = nameObj.first;
              q += " firstName='" + nameObj.first.replace(/'/g, "\\'") + "', lastName='" + nameObj.last.replace(/'/g, "\\'") + "', korean=0, ";
            }
            else {
              var mainName = nameObj.name;
              q += " kname='" + nameObj.name + "', korean=1, ";
            }
            $("#loading-text").html("Importing " + mainName + "...");
            break;
          case 'gender':
            q += " gender='" + (value=='남' ? 'M': 'F') + "', ";
            break;
          case 'moon':
            q += " moon = '" + (value=='음' ? 1: 0) + "', ";
            break;    
          case 'birthDate':
            value = this.convertFunkyDate(value);
            var converted = moment(value, 'M/D/YYYY').format("YYYY-MM-DD");
            if(converted.indexOf('00') ==0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'YYYY--M--D').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') < 0)
              q += " birthDate='" + converted + "', ";
            break;
          case 'baptismDate':
            value = this.convertFunkyDate(value);
            var converted = moment(value, 'M/D/YYYY').format("YYYY-MM-DD");
            if(converted.indexOf('00') ==0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'YYYY--M--D').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') < 0)
              q += " baptismDate='" + converted + "', ";
            break;
          case 'registerDate':
            value = this.convertFunkyDate(value);
            var converted = moment(value, 'M/D/YYYY').format("YYYY-MM-DD");
            if(converted.indexOf('00') ==0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'YYYY--M--D').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') == -1)
              q += " registerDate='" + converted + "', ";
            break;
          case 'status': 
            var val = "";
            if(value.indexOf('출석')>=0) val = '출석';
            else if(value.indexOf('장기출타')>=0) val = '장기출타';
            else if(value.indexOf('방문')>=0) val = '방문';
            else if(value.indexOf('이적')>=0) val = '이적';
            else if(value.indexOf('바나바케어')>=0) val = '바나바케어';
            q += " status = '" + val + "', ";
            if(value.indexOf('KM')>=0) val = 'KM';
            else if(value.indexOf('EM')>=0) val = 'EM';
            else if(value.indexOf('히스패닉')>=0 || value.indexOf('HM')>=0) val="HM";
            else val = "";
            q += " lanGroup = '" + val + "', ";
            break;
          case 'adultLevel':
            var val = value;
            if(value.indexOf('학교')>=0) val = '학생';
            q += " adultLevel = '" + val + "', ";
              break;
          case 'rankSince':
            value = this.convertFunkyDate(value);
            var converted = moment(value, 'M/D/YYYY').format("YYYY-MM-DD");
            if(converted.indexOf('00') ==0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'YYYY--M--D').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') == -1)
              q += " rankSince='" + converted + "', ";
            break;
          default:
            break;
        }
        return q;
      } else {
        return hinfo.cid + " = '" + value.toString().replace(/'/g, "\\'") + "', ";
      }
    },

    /************** IMPORT Visit OPERATIONS ***********/
    insertVisitCsv: function(line) {
      var cols = line.split('\t');
      var qstring = "";

      if(cols.length > 10) {
        var params = {};
        if(gCurrent.colOrder.indexOf(1) >=0) params.mid = cols[gCurrent.colOrder.indexOf(1)];
        if(gCurrent.colOrder.indexOf(14) >=0) params.location = cols[gCurrent.colOrder.indexOf(14)];
        if(gCurrent.colOrder.indexOf(2) >=0) params.memberName = cols[gCurrent.colOrder.indexOf(2)];
        if(gCurrent.colOrder.indexOf(6) >=0) params.visitorId = cols[gCurrent.colOrder.indexOf(6)];
        if(gCurrent.colOrder.indexOf(8) >=0) params.visitor2Id = cols[gCurrent.colOrder.indexOf(8)];
        if(gCurrent.colOrder.indexOf(5) >=0) params.visitor1 = cols[gCurrent.colOrder.indexOf(5)];
        if(gCurrent.colOrder.indexOf(7) >=0) params.visitor2 = cols[gCurrent.colOrder.indexOf(7)];
        if(gCurrent.colOrder.indexOf(4) >=0) params.visitDate = this.groomDate(cols[gCurrent.colOrder.indexOf(4)]);
        for(var i=0; i<cols.length; i++) {
          if(gColmap2[gCurrent.colOrder[i]-1].skip) {
            continue;
          } else {
            qstring += this.processColumnVisit(cols[i], angular.copy(gColmap2[gCurrent.colOrder[i]-1]));
          }
        }
        if(qstring) {
          params.qstring = qstring;
          webService.postData(gConfig.apiBase + "post/import/visit", params, true).then(function(result) {
            if(!live) console.log(result.success);
          });
        }
      }
    },
    /************** IMPORT Visit OPERATION (EXCEL) ***********/
    insertVisitExcel: function(data) {
      var qstring = "";
      var params = {};
      var i=0;
      for(var key in data) {
        if(key == '번호') params.mid = data[key];
        if(key == '심방장소') params.location = data[key];
        if(key == '대상자이름') params.memberName = data[key];
        if(key == '심방자1') params.visitor1 = data[key];
        if(key == '심방자2') params.visitor2 = data[key];
        if(key == '심방자1번호') params.visitorId = data[key];
        if(key == '심방자2번호') params.visitor2Id = data[key];
        if(key == '심방일자') params.visitDate = data[key];
        if(gColmap2[ gCurrent.colOrder[i]-1 ] && !gColmap2[ gCurrent.colOrder[i]-1 ].skip) {
          qstring += this.processColumnVisit(data[key], angular.copy(gColmap2[gCurrent.colOrder[i]-1]) );
          i++;
        } else {
          i++;
          continue; 
        }
      }
      if(qstring) {
        params.qstring = qstring;
        webService.postData(gConfig.apiBase + "post/import/visit", params, true).then(function(result) {
          if(!live) console.log(result.success);
        });
      }
    },

    groomDate: function(value) {
      value = this.convertFunkyDate(value);
      var converted = moment(value, 'M/D/YYYY').format("YYYY-MM-DD");
      if(converted.indexOf('00') ==0)
        converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
      if(converted.indexOf('Invalid') >=0)
        converted = moment(value, 'YYYY--M--D').format("YYYY-MM-DD");
      if(converted.indexOf('Invalid') < 0)
        return converted;
      else return value;
    },

    processColumnVisit: function(value, hinfo) {
      if(hinfo.special) {
        var q = "";
        switch(hinfo.cid) {
          case 'visitDate':
            value = this.convertFunkyDate(value);
            var converted = moment(value, 'M/D/YYYY').format("YYYY-MM-DD");
            if(converted.indexOf('00') ==0)
              converted = moment(value, 'M/D/YY').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') >=0)
              converted = moment(value, 'YYYY--M--D').format("YYYY-MM-DD");
            if(converted.indexOf('Invalid') < 0)
              q += " visitDate='" + converted + "', ";
            break;
          case 'visitMethod':
            q += " visitMethod = '" + (value.indexOf('전화') >=0 ? 1: 0) + "', ";
            break;
          default:
            break;
        }
        return q;
      } else {
        return hinfo.cid + " = '" + value.toString().replace(/'/g, "\\'") + "', ";
      }
    },
    convertFunkyDate: function(str) {
      if(typeof str=='string' && str.indexOf('--') >= 0) {
        str = str.replace("--", '-');
        str = str.replace("--", '-');
        return moment(str, "YYYY-M-D").format('M/D/YYYY');
      } else if(typeof str == 'number') {
        var thisDate=  new Date((str - (25567 + 1))*86400*1000);
        return moment(thisDate).format('M/D/YYYY');
      }
      else return str;
    }

  }
})
