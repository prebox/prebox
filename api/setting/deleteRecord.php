<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/addressFunctions.php");


if(is_numeric($id)) {
	switch($table) {
		case 'groups': $query = "DELETE from groups where groupId='$id'"; break;
		case 'user': $query = "DELETE from user where userId='$id'"; break;
		case 'sim': $query = "DELETE from sim where id='$id'"; break;
		default: break;
	}
	$success = false;
	if($table && $query) {
		$result = mysqli_query($link, $query);
		if(mysqli_affected_rows($link) > 0) {
			$success = true;
			switch($table) {
				/*
				case 'admin': logAdmin("DELETE", $id, $query); break;
				case 'training_class':logTraining("DELETE", $id, $query); break;*/
				default: break;
			}
		}
	}
} else {
	$success = false;
	$ids = json_decode($id, true);
	if($ids) {
		foreach($ids as $id){
			switch($table) {
				case 'groups': $query = "DELETE from groups where groupId='$id'"; break;
				case 'user': $query = "DELETE from user where userId='$id'"; break;
				case 'sim': $query = "DELETE from sim where id='$id'"; break;
				default: break;
			}
			if($query) {
				$result = mysqli_query($link, $query);
				if(mysqli_affected_rows($link) > 0) {
					$success = true;

					switch($table) {
						/*
						case 'admin': logAdmin("DELETE", $id, $query); break;
						case 'training_class':logTraining("DELETE", $id, $query); break; */
						default: break;
					}
				}
			}
		}
	}
}


print json_encode(array(
	'success'=> $success,
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'admin'=> $admin,
	'query'=> $testMode ? $query : false
	), 
JSON_NUMERIC_CHECK);

?>