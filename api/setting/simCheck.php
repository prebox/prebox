<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/addressFunctions.php");

$lines = explode("\n", $content);
$available = array();
$existing = array();
$invalid = array();

// Loop each sim lines
if(count($lines)) {
	// Process each line
	foreach($lines as $line) { 
		if(strstr($line, ",")) {
			// split the line into two parts separated by comma
			$tmp = explode(",", $line);

			if(count($tmp) ==2) { // Valid format
				$sim = $tmp[0];
				$code = $tmp[1];
				$query = "SELECT id from sim WHERE sim = '$sim' and carrierId = '$carrierId' and deleted = 0";
				$result = getdbquery($query);
				if($result) { // If found in record add to existing
					array_push($existing, $line);
				} else { // If not found in record add to available
					array_push($available, $line);
				}
			} else
				array_push($invalid, $line); // line in invalid format
		} else
			array_push($invalid, $line); // line in invalid format
	}
}

print json_encode(array(
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'available'=> $available,
	'existing'=> $existing,
	'lines'=>$lines,
	'invalid'=> $invalid,
	'query'=> $testMode ? $query : false
	), 
JSON_NUMERIC_CHECK);

?>