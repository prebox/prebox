'use strict';

var randInt = Math.floor(Math.random() * 999); 

var stateObj = {

  'login': {
    name: 'login',
    url: '/login',
    templateUrl: "views/login.html?" + randInt,
    controller: 'LoginCtrl',
    header: {
      nav: 'login',
    },
  },

  'forgot': {
    name: 'forgot',
    url: '/forgot',
    templateUrl: "views/forgot.html?" + randInt,
    controller: 'ForgotCtrl',
    header: {
      nav: 'login',
    }

  },

  /********** AGENT STATES **********/
  'app': {
    name: 'app',
    url: "",
    abstract: true,
    templateUrl: "views/agent/navigation.html?" + randInt,
    controller: 'AppCtrl',
    resolve: {
        result: function(pageResponse, $stateParams) { 
          return pageResponse.getResult(this.self);
        }
      }
  },

  'home': {
    name: 'home',
    url: "/agent/home",
    templateUrl: "views/agent/home.html?" + randInt,
    controller: 'aHomeCtrl',
    header: {
      title: 'Dashboard',
      nav: 'home'
    }
  },

  'groups': {
    name: 'groups',
    url: "/agent/groups",
    templateUrl: "views/agent/groups.html?" + randInt,
    controller: 'aGroupsCtrl',
    header: {
      title: 'Sub Accounts',
      nav: 'group'
    }
  },

  'account': {
    name: 'account',
    abstract: true,
    url: "",
    templateUrl: "views/agent/account.html?" + randInt,
    controller: 'aAccountCtrl',
    header: {
      nav: ''
    }
  },

  'general': {
    name: 'general',
    url: "/agent/account/general",
    templateUrl: "views/agent/account_general.html?" + randInt,
    controller: 'aAccountGeneralCtrl',
    header: {
      title: 'Account - General',
      nav: 'general'
    }
  },
  'billing': {
    name: 'billing',
    url: "/agent/account/billing",
    templateUrl: "views/agent/account_billing.html?" + randInt,
    controller: 'aAccountBillingCtrl',
    header: {
      title: 'Account - Billing',
      nav: 'billing'
    }
  },
  'users': {
    name: 'users',
    url: "/agent/account/users",
    templateUrl: "views/agent/account_users.html?" + randInt,
    controller: 'aAccountUsersCtrl',
    header: {
      title: 'Account - Users',
      nav: 'users'
    }
  },
  'rates': {
    name: 'rates',
    url: "/agent/account/rates",
    templateUrl: "views/agent/account_rates.html?" + randInt,
    controller: 'aAccountRatesCtrl',
    header: {
      title: 'Account - Rates',
      nav: 'rates'
    }
  },

  'sim': {
    name: 'sim',
    url: "/agent/setting/sim",
    templateUrl: "views/agent/sim.html?" + randInt,
    controller: 'aSettingSimCtrl',
    header: {
      title: 'SIM Management',
      nav: 'sim'
    }
  },

  /********** RETAILER STATES **********/
  'retailer': {
    name: 'retailer',
    url: "",
    abstract: true,
    templateUrl: "views/retailer/navigation.html?" + randInt,
    controller: 'RetailerCtrl',
    resolve: {
        result: function(pageResponse, $stateParams) { 
          return pageResponse.getResult(this.self);
        }
      }
  },

  'rhome': {
    name: 'rhome',
    url: "/retailer/home",
    templateUrl: "views/retailer/rhome.html?" + randInt,
    controller: 'rHomeCtrl',
    header: {
      title: 'Home',
      nav: 'home'
    }
  },

  'activate': {
    name: 'activate',
    url: "/retailer/activate",
    templateUrl: "views/retailer/activate.html?" + randInt,
    controller: 'rActivateCtrl',
    header: {
      title: 'Activate SIM',
      nav: 'activate'
    }
  }
}
