'use strict';


// Declare app level module which depends on views, and components
angular.module('prebox', [
  'ngRoute',
  'ui.router',
  'services.global',
  'ui.checkbox',
  'oc.lazyLoad',
  'angularUtils.directives.dirPagination',
  'prebox.mainControllers'
]).
run(function($rootScope, $templateCache, $state, gService, webService, $ocLazyLoad) {
  if(!live) console.log("run choong");

  $rootScope.imageBase = gConfig.imageBase;

  // Remove cache
  $rootScope.$on('$viewContentLoaded', function() {
      $templateCache.removeAll();
   });
  $rootScope.randomVar = Math.floor((Math.random() * 99999) + 1);
  $rootScope.imageVar = Math.floor((Math.random() * 99999) + 1);
  $rootScope.updateRand = function() {
    $rootScope.imageVar = Math.floor((Math.random() * 99999) + 1);
  }

  /********** GLOBAL ROOT FUNCTIONS ********/
  $rootScope.headerUrl = "views/header.html?" + $rootScope.randomVar;
  $rootScope.accountHeaderUrl = "views/agent/accountHeader.html?" + $rootScope.randomVar;
  $rootScope.live = gConfig.isLive;


  // Menu click event
  $rootScope.menuClick = function() {
    $('div.main').toggleClass('menu');
  }
  // State/Page change
  $rootScope.nav = function(state, param) {
    $state.go(state, param);
  }
  $rootScope.checkEmptyObject = function(obj) {
    if(!obj) return 1;
    return Object.keys(obj).length == 0;
  }
  $rootScope.checkAgent = function() {
    if(gCurrent.group)
      return gCurrent.group.groupType == 'AGENT';
    else
      return false;
  }
  $rootScope.loadUser = function(data) {
    if(data.user)
      gCurrent.groupId = data.user.groupId;
    if(data.group) {
      gCurrent.mygroup = data.group;
      gCurrent.group = data.group;
    }
    if(data.groups) {
      gCurrent.groups = data.groups;
    }

    /********** LAZY LOAD *************/
    if(data.groupType == 'AGENT') {
      $('body').removeClass('retailer');
      if(!gCurrent.agentLoaded) {
        $ocLazyLoad.load({
          rerun: true,
          files: ['controllers/agent/groupControllers.js', 
              'controllers/agent/accountControllers.js',
              'controllers/agent/settingControllers.js']
        });
        gCurrent.agentLoaded = true;
      }
    } else if (!gCurrent.retailerLoaded) {
      $('body').addClass('retailer');
      if(!gCurrent.retailerLoaded) {
        $ocLazyLoad.load({
          rerun: true,
          files: ['controllers/retailer/retailerControllers.js']
        });
        gCurrent.retailerLoaded = true;
      }
    }

  }
  /* Language and Text handling */
  $rootScope.local = function(path, primary, var1, var2) {
    function findPath(path, remaining) {
      var i = remaining.indexOf('.');
      if(i>=0) {
        var arr = [remaining.slice(0,i), remaining.slice(i+1)];
        var txt = path[arr[0]];
        if(typeof txt == 'object')
          return findPath(txt, arr[1]);
      }
      else 
        return path[remaining];
    }
    var lan = gSetting.lan;
    if(lan=='default' || !lan) {
      if(primary) lan = primary;
      else lan = 'en';
    } 
    var val = findPath( gLocal[lan], path);
    if(!val) return "";
    if(var1)  {
      val = val.toString().replace('{{var}}', var1);
    }
    if(var2) {
      val = val.toString().replace('{{var2}}', var2);
    }
    return val;
  }
  $rootScope.openLink = function(url) {
    var win = window.open(url, '_blank');
    win.focus();
  }
  $rootScope.message = function(type, title, content, callback) {
    this.callback = callback;
    $('.modal-body.message .button-container').hide();
    $('.modal-body.message .button-container.' + type).show();
    $('.modal-body.message .title').html(title);
    $('.modal-body.message .content').html(content);
    
    this.toggleModal('message');
    //focus on button so you can exit with enter button
    setTimeout(function() {
      if(type == 'message')
       $('.modal-body.message .button-container > button').focus();
      else if(type == 'confirm') 
        $('.modal-body.message .button-container > button.ok').focus();
    }, 100);
    
  }
  $rootScope.encodeAddress = function(formatted) {
    return formatted.split(' ').join('+');
  }
  $rootScope.toggleModal = function(id) {
    $('.modal-body').hide();
    $('.modal-body.' + id).show();
    $rootScope.modalShown = !$rootScope.modalShown;
  };
  $rootScope.address2line = function(formatted) {
    return formatted.replace(',', '<br>');
  }
  $rootScope.formatDate = function(time, fm) {
    if(!time) return "";
    var formatted =  moment(time, "YYYY-MM-DD").format(fm);
    if(formatted.indexOf('Invalid')>=0)
      return time;
    else
      return formatted;
  };
  $rootScope.formatDate2 = function(time, fm) {
    if(!time) return "";
    var formatted = moment(time, "M/D/YYYY").format(fm);
    if(formatted.indexOf('Invalid')>=0)
      return time;
    else
      return formatted;
  };
  $rootScope.formatTime = function(time, fm) {
    if(!time) return "";
    var formatted = moment(time, "YYYY-MM-DD HH:mma").format(fm);
    if(formatted.indexOf('Invalid')>=0)
      return time;
    else
      return formatted;
  };
  $rootScope.stampToDate = function(value, fm) {
    if(!fm) fm = "h:mma (M/D)";
    return moment.unix(value).format(fm);
  };
  $rootScope.backClick = function() {
    if(!live) console.log("BACK");
    /*
    console.log($state.current);
    if($state.current.header && $state.current.header.back)
      $state.go($state.current.header.back);
    else
      */
    window.history.back();
  }
  $rootScope.showLoading = function(text) {
    $('#loading-text').html(text ? text : 'Loading...');
    $('body').addClass('loading');
  }
  $rootScope.hideLoading = function() {
    $('body').removeClass('loading');
  }
  $rootScope.delete = function(table, id, callback) {
    var localThis = this;
    this.message('confirm', $rootScope.local('msg.delete', 'en'), $rootScope.local('msg.delete2', 'en'), function() {
      webService.postData(gConfig.apiBase + 'delete/general/delete', { table: table, id: id}).then(function(result) {
        if(result.success) {
          if(callback) callback(result);
          else localThis.backClick();
        }
        else
          localThis.message('message', "Error", "Delete operation failed");
      })
    })
  }

  /* STATE CHANGE */
  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      $('body').scrollTop(0);
      $rootScope.modalShown = false;
  });
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, $rootScope) {
      // Back to login when trying to access app without Login

      if (toState.name != "login" && toState.name != "forgot") {
        gCurrent.userId = gService.getCookie('userId');
        gCurrent.level = gService.getCookie('level');
        gCurrent.userId = +gCurrent.userId;
        gCurrent.level = +gCurrent.level;

        if (!gCurrent.userId || !gCurrent.level) {
          event.preventDefault();
          $state.go('login');
        }
      } 
      else {
        gCurrent.userId = 0;
        gCurrent.level = 0;
      }
  });

})


.config(['$stateProvider', '$urlRouterProvider', 'paginationTemplateProvider', function($stateProvider, $urlRouterProvider, paginationTemplateProvider) {
  paginationTemplateProvider.setPath('views/partial/dirPagination.tpl.html');

  var defaultPath = 'login';
  
  /**************** ROUTERS ****************/
  $stateProvider
    .state('app', stateObj.app)
    .state('login', stateObj.login)
    .state('forgot', stateObj.forgot)
    .state('retailer', stateObj.retailer)

    // AGENT
  	.state('app.home', stateObj.home)
    .state('app.groups', stateObj.groups)
    .state('app.account', stateObj.account)
    .state('app.account.general', stateObj.general)
    .state('app.account.users', stateObj.users)
    .state('app.account.rates', stateObj.rates)
    .state('app.account.billing', stateObj.billing)
    .state('app.sim', stateObj.sim)
    // RETAILER
    .state('retailer.rhome', stateObj.rhome)
    .state('retailer.activate', stateObj.activate)

  $urlRouterProvider.otherwise(defaultPath);
}])

.factory('webService', function($q, $rootScope, $state) {
  return {
    postData: function(url, param, hideLoading) {
      var userId = localStorage.getItem('userId');
      if(!param) param = {};
      var result = $q.defer();
      if(!hideLoading) {
        $rootScope.showLoading();
      }
      if(!live) console.log("param", param);
      $.retryAjax({
        url: url,
        timeout: 5000,
        retryLimit: 3,
        headers: {
          'Authtoken': localStorage.getItem('authtoken'),
          'deviceId': localStorage.getItem('deviceId'),
          'userId': gCurrent.userId ? gCurrent.userId : 0
        },
        method: 'post',
        data: param,
        success: function(response) {
          if(!live) console.log("******* SERVICE CALL (POST) - " + url);
          if(!hideLoading) $rootScope.hideLoading();
          try{
            if(!param.nojson)
              response = JSON.parse(response);
            if(!live && !param.nojson) console.log("RESPONSE: ", response);
            if(response.authDenied)
              $state.go('login');
            else
              result.resolve(response);
          }catch(error) {
            $rootScope.hideLoading();
            alert("Server Error. Operation aborted.")
          }
        },
        error: function(err) {
          $rootScope.hideLoading();
          result.resolve(false);
        }
      });
      return result.promise;
    },
    getData: function(url, param, hideLoading) {
      var userId = localStorage.getItem('userId');
      if(!param) param = {};
      var result = $q.defer();
      if(!hideLoading) {
        $rootScope.showLoading();
      }
      var paramStr = jQuery.param( param );
      if(paramStr)
        url = url + '?' + paramStr;
      $.retryAjax({
        url: url,
        timeout: 5000,
        retryLimit: 3,
        headers: {
          'Authtoken': localStorage.getItem('authtoken'),
          'deviceId': localStorage.getItem('deviceId'),
          'userId': gCurrent.userId ? gCurrent.userId : 0
        },
        method: 'get',
        success: function(response) {
          if(!live) console.log("******* SERVICE CALL (GET) - " + url);
          if(!hideLoading) $rootScope.hideLoading();
          try {
            if(!param.nojson)
              response = JSON.parse(response);
            if(!live && !param.nojson) console.log("RESPONSE: ", response);
            if(response.authDenied)
              $state.go('login');
            else
              result.resolve(response);
          } catch(error) {
            $rootScope.hideLoading();
            alert("Server Error. Operation aborted.")
          }
        },
        error: function(err) {
          $rootScope.hideLoading();
          result.resolve(false);
        }
      });
      return result.promise;
    }
  }
})

// Fetch necessary data for each view. 
// Every view first checks whether client info is fetched before doing anything
.service('pageResponse', function($q, $http, $state, webService, $rootScope) {
  return {
    getResult: function(pageParam, id, id2) {
      var result = $q.defer();
      if (!gInit) {
        webService.getData(gConfig.apiBase + "get/init")
          .then(function(data) {
              gInit = true;
              $rootScope.loadUser(data);
              // Retrieve any other web calls for the page defined in global.js
              if (pageParam.service) {
                var service = pageParam.service;
                if(!service.data) service.data = {};
                if(id != undefined) service.data.id = id;
                if(id2 != undefined) service.data.id2 = id2
                webService.getData(service.url + (id!=null?'/'+id:'') + (id2!=null ? '/' + id2: '')).then(function(data) {
                  result.resolve(data);
                })
              }
              else 
                result.resolve(false);
          });
      } else {
        // Web service call for page-specific
        if (pageParam.service) {
          var service = pageParam.service;
          if(!service.data) service.data = {};
          if(id != undefined) service.data.id = id; 
          if(id2 != undefined) service.data.id2 = id2;
          webService.getData(service.url + (id!=null?'/'+id:'') + (id2!=null ? '/' + id2: '')).then(function(data) {
            result.resolve(data);
          })
        } else {
          result.resolve(false);
        }
      }

      return result.promise;
    },
  }
})

.filter('to_trusted', ['$sce', function($sce){
  return function(text) {
    return $sce.trustAsHtml(text);
  };
}])

.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return val != null ? parseInt(val, 10) : null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
})

.directive('modalDialog', function() {
  return {
    restrict: 'E',
    scope: {
      show: '='
    },
    replace: true, // Replace with the template below
    transclude: true, // we want to insert custom content inside the directive
    link: function(scope, element, attrs) {
      scope.dialogStyle = {};
      if (attrs.width)
        scope.dialogStyle.width = attrs.width;
      if (attrs.height)
        scope.dialogStyle.height = attrs.height;
      scope.hideModal = function() {
        scope.show = false;
      };
    },
    template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
  };
});
