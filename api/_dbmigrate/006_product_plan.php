<?php

$query = "
CREATE TABLE `prebox`.`product_plan` ( 
	`id` INT NOT NULL AUTO_INCREMENT , 
	`productId` INT NOT NULL , 
	`name` VARCHAR(63) NOT NULL , 
	`price` DECIMAL(10,2) NOT NULL , 
	`createdAt` INT NOT NULL , 
	`updatedAt` INT NOT NULL , 
	`deleted` BOOLEAN NOT NULL , 
	PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
";

?>
