<?php
$api_key = 'AIzaSyA8l29Soj99o4yddmEaz4hoOQ74EtMYtic';

function encodeAddress($street, $city, $state, $zip) {
	$address =  $street.",".($city ? $city.",": "").($state ? $state." " : "").($zip ? $zip : "");
	return urlencode($address);	
}

function getAddress($street, $city, $state, $zip, $formatted="") {
	global $api_key;
	if($formatted) 
		$address = urlencode($formatted);
	else 
		$address = encodeAddress($street, $city, $state, $zip);

	// Get cURL resource
	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1, 
		CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?key='.$api_key.'&address='.$address,
	));
	// Send the request & save response to $resp
	$resp = curl_exec($curl);
	// Close request to clear up some resources
	curl_close($curl);
	 
	$resp = json_decode($resp, true);
	$result = $resp['results'][0];

	if($result) {
		$address = insertAddress($result);
		return $address;
	} else 
		return false;
}

function insertAddress($result) {
	global $link;
	$placeId = $result['place_id'];
	
	$query = "select * from address where placeId='$placeId'";
	$address = getdbquery($query);
	if($address) 
		return $address;
	
	elseif(!$address) {
		$addressComps = $result['address_components'];
		$formatted = $result['formatted_address'];
		$geometry = $result['geometry']['location'];
		$address = array();
		
		foreach($addressComps as $comp) {
			if(in_array("street_number", $comp['types']))
				$address['street'] = $comp['short_name'];
			elseif(in_array("route", $comp['types']))
				$address['route'] = $comp['short_name'];
			elseif(in_array("locality", $comp['types']))
				$address['city'] = $comp['long_name'];
			elseif(in_array("administrative_area_level_1", $comp['types']))
				$address['state'] = $comp['short_name'];
			elseif(in_array("administrative_area_level_2", $comp['types']))
				$address['county'] = $comp['long_name'];
			elseif(in_array("postal_code", $comp['types']))
				$address['zip'] = $comp['short_name'];
		}
		$address['street'] = $address['street'].' '.$address['route'];
		$address['lat'] = $geometry['lat'];
		$address['lng'] = $geometry['lng'];
		$address['placeId'] = $placeId;
		
		$formatted = str_replace(", USA", "", $formatted);
		$address['formatted'] = $formatted;
		
		$query = "insert into address set placeId='$placeId', 
				street = '".$address['street']."', 
				city = '".$address['city']."',
				state = '".$address['state']."',
				zip = '".$address['zip']."',
				county = '".$address['county']."',
				lat = '".$address['lat']."',
				lng = '".$address['lng']."',
				formatted='".addslashes($formatted)."'";
		mysqli_query($link, $query); 
		if(mysqli_affected_rows($link)<0) {
			$query = "select * from address where placeId = '$placeId'";
			$tmp = getdbquery($query);
			$address['addressId'] = $tmp['addressId'];

		} else 
			$address['addressId'] = mysqli_insert_id($link);
	}
	if(!$address['addressId']) return $mysqli_query;
	return $address;
}

?>