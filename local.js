
/***************** ENGLISH ************************/


var gLocal = {
    'en': {
        'appName': 'PREBOX',
        'loading': 'Loading...',
        'submit': 'Submit',
        'delete': 'Delete',
        'cancel': 'Cancel',
        'age': 'years',
        'empty': 'No data',
        'login': {
            'title': 'Admin Login',
            'email': 'Email Address',
            'username': 'User Name',
            'password': 'Password',
            'securitycode': 'Security Code',
            'remember': 'Remember Me',
            'button': 'LOGIN NOW',
            'forgot': 'Forgot Password?'
        },
        'forgot': {
            'title': 'Forgot Password',
            'button1': 'Verify Email',
            'fail_title': 'Alert',
            'button2': 'Submit',
            'button3': 'Submit'
        },  
        'msg': {
            'success': 'Success',
            'delete': 'Delete?',
            'delete2': 'You cannot undo this operation. Proceed to delete?',
            'group_success': 'Group {{var}} successfully',
            'group_delete': 'This will also remove all its sub-group. Proceed?',
            'logout_confirm': 'Logout',
            'logout_confirm2': 'Continue to log out?',
            'logout_loader': 'Logging out...',
            'setting_success': 'Setting successfully modified',
            'failed': 'No Update',
            'failed2': 'Nothing added or updated.',
            'admin_success': 'Admin successfully {{var}}',
            'admin_fail': 'Double check if admin with same email already exists.',
            'login_loader': 'Logging in...',
            'login_fail': 'Login failed',
            'login_fail2': 'Email and password do not match our record.<br>Please try again',
            'member_loader': 'Submitting data...',
            'member_success': '{{var}} successfully {{var2}}',
            'import_failed': 'Only .xls, .xlsx are allowed',
            'ok': 'OK',
            'cancel': 'Cancel'
        }
    }
}