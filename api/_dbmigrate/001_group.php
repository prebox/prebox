<?php

$query = "ALTER TABLE `groups` 
	ADD `accountId` VARCHAR(15) NOT NULL AFTER `r`, 
	ADD `taxId` VARCHAR(15) NOT NULL AFTER `accountId`,
	ADD `addressId` INT NOT NULL AFTER `accountId`, 
	ADD `formatted` VARCHAR(127) NOT NULL AFTER `addressId`, 
	ADD `mobile` VARCHAR(15) NOT NULL AFTER `formatted`, 
	ADD `contactName` VARCHAR(127) NOT NULL AFTER `mobile`, 
	ADD `email` VARCHAR(127) NOT NULL AFTER `contactName`, 
	ADD `balance` DECIMAL(10,2) NOT NULL AFTER `groupType`,
	ADD `status` VARCHAR(15) NOT NULL DEFAULT 'ACTIVE' AFTER `email`;";
	
?>
