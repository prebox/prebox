'use strict';

angular.module('prebox.accountControllers', [])

.controller('aAccountCtrl', function($scope, $state, $rootScope, gService) {
    $scope.currentheader = $state.current.header;
    $scope.$parent.navChange($scope.currentheader);
    $scope.group = gService.getCurrentGroup();

    $scope.init = function() {
    	//$scope.changeTab('general');
    }
    $scope.$watch('group', function(group) {
        $scope.$parent.group = group;
    })
    $scope.changeTab = function(type) {
    	$('.submenu > li').removeClass('active');
    	$('.submenu > li.' + type).addClass('active');
    	$('.account-page .content').hide();
    	$('.account-page .content.' + type).show();
    	$state.go('app.account.' + type, {}, {location:'replace'});
    }
    $scope.init();

})

/****************** ACCOUNT GENERAL *********************/
.controller('aAccountGeneralCtrl', function($scope, $state, $rootScope, webService, gService) {
    $scope.header = $state.current.header;
    $scope.$parent.$parent.navChange($scope.currentheader);
    $scope.general = angular.copy($scope.$parent.group);
    $scope.$parent.changeTab('general');

    // Update and Verify Address on change
    $('input.address').change(function(e) {
      var thisInput = $(this);
      var attr = thisInput.attr('data-id');
      var address = thisInput.val();
      if(address) {
        webService.postData(gConfig.apiBase + 'post/address/verify', { address: address}).then(function(result) {
            if(result.success) {
                $scope.general[attr] = result.address.addressId;
                $scope.general['formatted'] = result.address.formatted;
            }
        });
      }
    })
    $scope.submit = function() {
        $scope.general.groupId = gCurrent.group.groupId;
        webService.postData(gConfig.apiBase + 'post/general', $scope.general ).then(function(result) {
          if(result.success) {
            $scope.general = angular.copy(result.group);
            $scope.$parent.group = result.group;
            $rootScope.message('message', "Saved successfully!", "");
          }
          else {
            $rootScope.message('message', "Not Saved", "Probably nothing has been changed");
          }
        })
    }
})



.controller('aAccountBillingCtrl', function($scope, $state, $rootScope, webService, gService) {
    $scope.header = $state.current.header;
    $scope.$parent.navChange($scope.header);
    $scope.$parent.changeTab('billing');

})

.controller('aAccountUsersCtrl', function($scope, $state, $rootScope, webService, gService) {
    $scope.header = $state.current.header;
    $scope.$parent.navChange($scope.header);
    $scope.$parent.changeTab('users');

    $scope.groupId = gCurrent.group.groupId;

    $scope.init = function() {
        $scope.page = {
            size: 100,
            total: null,
            current: 1
      }
      $scope.membersSelected = {};
      $scope.sortOrder = 0;
      $scope.getData(1);
    }

    /* GET training data */
    $scope.getData = function(newPage, exportFlag) {
        // Keyword Search
        if(exportFlag) {
          var filePath = gConfig.apiBase + '/account/getUsers.php?authtoken=' + localStorage.getItem('authtoken') + '&groupId=' + $scope.groupId +'&exportFlag=1';
          console.log(filePath);
          window.location = filePath;
          return ;
        }
        webService.postData(gConfig.apiBase + 'post/users', 
            { groupId: $scope.groupId, 
              psize: $scope.page.size, pcurrent: newPage, sortKey: $scope.sortKey, sortOrder: $scope.sortOrder }).then(function(result) {
            $scope.newResult(result); 
        });
    }
    $scope.newResult = function(result) {
      $scope.page.total = result.total;
      $scope.users = result.users;
    }
    $scope.sort = function(id) {
      if($scope.sortKey == id) {
        // Reverse sortorder
        $scope.sortOrder = 1 - $scope.sortOrder;
      } else {
        $scope.sortKey = id;
        $scope.sortOrder = 0;
      }
      $scope.getData(1);
    }
    $scope.exportResult = function() {
      $scope.getData(1, 1);
    }

    /* REMOVING FROM GROUP */
    $scope.selectAll = function() {
      $scope.membersSelectAll = !$scope.membersSelectAll;
      if($scope.membersSelectAll ) {
        $( "input.member" ).prop('checked', true);
        $( "input.member" ).each(function( index ) {
          $scope.membersSelected[index] = +($(this).attr('ng-true-value'));
        });
      } else {
        $( "input.member" ).prop('checked', false);
        $scope.membersSelected = {};
      }
    }
    $scope.selectMember = function(index) {
      if(!$scope.membersSelected[index]) {
        delete $scope.membersSelected[index];
      }
    }

    // Add New Account popup
    $scope.addNewUser = function() {
      $scope.user =  {}
      $rootScope.toggleModal('addnewuser');
    }
    $scope.editUser = function(user) {
        $scope.user = user;
        $rootScope.toggleModal('addnewuser');
    }
    $scope.deleteMember = function() {
      var id = JSON.stringify($scope.membersSelected);
      $rootScope.delete('user', id, function() {
        $scope.getData(1);
      });
    }

    $scope.submitAdd = function() {
        $scope.user.groupId = $scope.groupId;
      webService.postData(gConfig.apiBase + 'post/user', $scope.user ).then(function(result) {
          $rootScope.modalShown = false;
          if(result.success) {
            $rootScope.message('message', ($scope.user.userId ? "Updated" : "Added") + "successfully!", "");
            $scope.getData(1);
          }
          else {
            $rootScope.message('message', ($scope.user.userId ? "Update" : "Add") + "failed", result.errMsg);
          }
      });
    }
    $scope.init();

})

.controller('aAccountRatesCtrl', function($scope, $state, $rootScope, webService, gService) {
    $scope.header = $state.current.header;
    $scope.$parent.navChange($scope.header);
    $scope.$parent.changeTab('rates');

})

