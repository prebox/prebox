<?php

include_once(dirname(__FILE__)."/../config.php");


if(!$psize) $psize = 100;
if(!$pcurrent) $pcurrent = 1;
$pcurrent--;
$pcurrent = $pcurrent * $psize;

$filterStr = "";

if(!$sortKey) $sortKey = "createdAt";
if(!$sortOrder) $sortOrder = "ASC";
else $sortOrder = "DESC";

if($groupId) {
	if($mobile) {
		$sel = "";
		$queryExtend = ""; 
	}
	else {
		if($exportFlag) $queryExtend = ""; 
		$sel = "";
		$queryExtend = " LIMIT $pcurrent, $psize"; 
	}

	$filterStr = " and groupId = '$groupId' ";

	$queryMain = "SELECT SQL_CALC_FOUND_ROWS  * FROM sim WHERE deleted =0 $filterStr
		ORDER BY $sortKey $sortOrder";
	
	$rows = getdbrows($queryMain.$queryExtend);
	$result = getdbquery("SELECT FOUND_ROWS() as total");
	$total = $result['total'];

} else
	$rows = false;


/*
if($exportFlag) {
	$file_ending = "csv";
	header('Content-Encoding: UTF-8');
	header("Content-type: text/csv; charset=UTF-8");
	header("Content-Disposition: attachment; filename=".$group['name'].".csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo "\xEF\xBB\xBF";

	$fp = fopen('php://output', 'w');
	$arr = array("groupId", "name", "type", "createdAt");

	fputcsv($fp, $arr);
	foreach($rows as $row) {
		//fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		$cell = jsonParse($row['cell']);
		$tmp = array($row['groupId'], $row['name'], $row['groupType'], $row['createdAt'] );
		fputcsv($fp, $tmp);
	}
	fclose($fp);
	exit();
}
*/

print json_encode(array(
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'sims'=> $rows,
	'total'=>$total,
	'query'=> $testMode ? $queryMain : false
	), 
JSON_NUMERIC_CHECK);

?>