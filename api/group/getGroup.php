<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/groupFunctions.php");

if($groupId) {
	$group = getGroup($groupId);
}

print json_encode(array(
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'group'=>$group,
	'query'=> $testMode ? $queryMain : false
	), 
JSON_NUMERIC_CHECK);

?>