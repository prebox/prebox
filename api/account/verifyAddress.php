<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/addressFunctions.php");


$add = getAddress("", "", "", "", $address);

if($add) {
	$success = true;
	if(strstr($address, '#'))
		$add['formatted'] = $address;
} else {
	$success = false;
}

print json_encode(array(
	'success'=> $success,
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'address'=> $add
	), 
JSON_NUMERIC_CHECK);

?>