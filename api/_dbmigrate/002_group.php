<?php

$query = "
ALTER TABLE `user` ADD `email` VARCHAR(63) NOT NULL AFTER `name`;
ALTER TABLE `user` ADD `status` VARCHAR(15) NOT NULL DEFAULT 'ACTIVE' AFTER `level`;
ALTER TABLE `user` ADD UNIQUE(`username`);
ALTER TABLE `user` CHANGE  `security_code`  `security_code` VARCHAR( 40 ) NOT NULL ;
";

?>
