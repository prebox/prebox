'use strict';

angular.module('prebox.retailerControllers', [])

.controller('rHomeCtrl', function($scope, $state, $rootScope) {
    $scope.header = $state.current.header;
    $scope.$parent.navChange($scope.header);

})

.controller('rActivateCtrl', function($scope, $state, $rootScope, gService) {
    $scope.currentheader = $state.current.header;
    $scope.$parent.navChange($scope.currentheader);
    $scope.group = gService.getCurrentGroup();

    $scope.init = function() {
    	//$scope.changeTab('general');
    }
    $scope.$watch('group', function(group) {
        $scope.$parent.group = group;
    })
    $scope.changeTab = function(type) {
    	$('.submenu > li').removeClass('active');
    	$('.submenu > li.' + type).addClass('active');
    	$('.account-page .content').hide();
    	$('.account-page .content.' + type).show();
    	$state.go('app.account.' + type, {}, {location:'replace'});
    }
    $scope.init();

})
