<?php

include_once(dirname(__FILE__)."/config.php");

extract($_GET);

if($method == 'GET') {
	switch($path) {
		case 'member': 			$fp = $rootPath."/member/getMember.php"; break;
		case 'init': 			$fp = $rootPath."/init.php"; break;
		case 'test': 			$fp = $rootPath."/test.php"; break;
		case 'user':			$fp = $rootPath."/account/getUser.php"; break;
		default: 				$fp = $rootPath."/notfound.php"; break;
	}
} else {
	switch($path) {
		// PAGINATION POSTS
		case 'groups': 			$fp = $rootPath."/group/getGroups.php"; break;
		case 'users': 			$fp = $rootPath."/account/getUsers.php"; break;
		case 'sims': 			$fp = $rootPath."/setting/getSims.php"; break;
		// REGULAR POSTS
		case 'forgot1': 		$fp = $rootPath."/forgot/step1.php"; break;
		case 'forgot3': 		$fp = $rootPath."/forgot/step3.php"; break;
		case 'address/verify': 	$fp = $rootPath."/account/verifyAddress.php"; break;
		case 'general': 		$fp = $rootPath."/account/saveGeneral.php"; break;
		case 'user': 			$fp = $rootPath."/account/saveUser.php"; break;
		case 'simcheck':		$fp = $rootPath."/setting/simCheck.php"; break;
		case 'simsave':			$fp = $rootPath."/setting/simSave.php"; break;

		case 'login': 			$fp = $rootPath."/login.php"; break;
		case 'logout': 			$fp = $rootPath."/logout.php"; break;
		// DELETES
		case 'group/delete': 	$fp = $rootPath."/group/deleteGroup.php"; break;
		case 'group/removeMember': 	$fp = $rootPath."/group/removeFromGroup.php"; break;
		case 'search/delete': 	$fp = $rootPath."/member/deleteFilter.php"; break;
		case 'general/delete': 	$fp = $rootPath."/setting/deleteRecord.php"; break;
		case 'class/remove': 	$fp = $rootPath."/training/removeFromClass.php"; break;
		default: 				$fp = $rootPath."/notfound.php"; break;
	}
}

include_once($fp);


?>