<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/groupFunctions.php");


if(!$psize) $psize = 100;
if(!$pcurrent) $pcurrent = 1;
$pcurrent--;
$pcurrent = $pcurrent * $psize;

if(!$sortKey) $sortKey = "name";
if(!$sortOrder) $sortOrder = "ASC";
else $sortOrder = "DESC";

if($groupId) {
	$group = getGroup($groupId);

	if($mobile) {
		$sel = "";
		$queryExtend = ""; 
	}
	else {
		if($exportFlag) $queryExtend = ""; 
		$sel = "";
		$queryExtend = " LIMIT $pcurrent, $psize"; 
	}

	if($self)
		$whereStr = "(g1.l >= '".$group['l']."' and g1.r <= '".$group['r']."') ";
	else
		$whereStr = "(g1.l > '".$group['l']."' and g1.r < '".$group['r']."') ";

	$queryMain = "SELECT SQL_CALC_FOUND_ROWS 
	 *
		$sel
		FROM groups as g1
		WHERE
			g1.deleted =0 and
			$whereStr
		GROUP BY g1.groupId
		ORDER BY $sortKey $sortOrder";
	
	$rows = getdbrows($queryMain.$queryExtend);
	$result = getdbquery("SELECT FOUND_ROWS() as total");
	$total = $result['total'];

	$midList = array();
	$emailList = array();
	$noemailList = array();
	if($rows) {
		foreach($rows as $row) {
			array_push($midList, $row['mid']);
		}
	}

} else
	$rows = false;


if($exportFlag) {
	$file_ending = "csv";
	header('Content-Encoding: UTF-8');
	header("Content-type: text/csv; charset=UTF-8");
	header("Content-Disposition: attachment; filename=".$group['name'].".csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo "\xEF\xBB\xBF";

	$fp = fopen('php://output', 'w');
	$arr = array("groupId", "name", "type", "createdAt");

	fputcsv($fp, $arr);
	foreach($rows as $row) {
		//fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		$cell = jsonParse($row['cell']);
		$tmp = array($row['groupId'], $row['name'], $row['groupType'], $row['createdAt'] );
		fputcsv($fp, $tmp);
	}
	fclose($fp);
	exit();
}


print json_encode(array(
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'groups'=> $rows,
	'midList'=>$midList,
	'emailList'=> $emailList,
	'sortOrder'=>$sortOrder,
	'noemailList'=> $noemailList,
	'total'=>$total,
	'group'=>$group,
	'query'=> $testMode ? $queryMain : false
	), 
JSON_NUMERIC_CHECK);

?>