<?php

include_once(dirname(__FILE__)."/../config.php");
include_once(dirname(__FILE__)."/../_lib/addressFunctions.php");

$lines = explode("\n", $content);
$saved = array();
$failed = array();
$success = false;

// Loop each sim lines
if(count($lines)) {
	// Process each line
	foreach($lines as $line) { 
		if(strstr($line, ",")) {
			// split the line into two parts separated by comma
			$tmp = explode(",", $line);

			if(count($tmp) ==2) { // Valid format
				$sim = $tmp[0];
				$code = $tmp[1];
				$query = "INSERT INTO sim SET carrierId = '$carrierId', sim='$sim', code='$code', groupId='$groupId', createdAt='$now'";
				mysqli_query($link, $query);
				if(mysqli_affected_rows($link) >= 1) { // If found in record add to existing
					array_push($saved, $line);
					$success = true;
				} else { // If not found in record add to available
					array_push($failed, $line);
				}
			} else
				array_push($failed, $line); // line in invalid format
		} else
			array_push($failed, $line); // line in invalid format
	}
}

print json_encode(array(
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'saved'=> $saved,
	'failed'=> $failed,
	'lines'=>$lines,
	'success'=>$success,
	'query'=> $testMode ? $query : false
	), 
JSON_NUMERIC_CHECK);

?>