<?php

function array_to_xml( $data, &$xml_data ) {
    $exclusions = array('addressId', 'txCode', 'promoId', 'autoPrinted', 'categoryId', 'imagePath', 'imageUrl', 'hashKey', 'regId', 'cardId', 'promoId');
    foreach( $data as $key => $value ) {
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            if($value != null && !in_array($key, $exclusions)) 
            {
                if($key == 'totalPrice' || $key == 'price')
                    $value = number_format($value, 2);
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
     }
}

?>