<?php

include_once(dirname(__FILE__)."/../config.php");


// $email

$query = "SELECT * from admin where email='$email' and deleted=0";
$admin = getdbquery($query);
if($admin) {
	$adminId = $admin['adminId'];
	$code = mt_rand(1000, 9999);
	$fromemail = "no-reply@antiochurch.org";
	$fromname = "Antioch Admin";
	$emailto = $admin['email'];
	$subject = "Please confirm your identiy";
	$bodytext = 'Hi,<br><br>
	Please confirm your identity by entering the following confirmation code to the website form
	<br><br>'.$code.'<br><br>Thank you<br><br>';
	
	send_email($fromemail, $emailto, $subject, $bodytext, $fromname);
	$success = true;
}
else {
	$code = false;
	$adminId = false;
	$success = false;
	$errMsg = "Email ".$email." cannot be found in the records. Please contact admin for help.";
}	


print json_encode(array(
	'success'=>$success,
	'adminId'=>$adminId,
	'code'=>$code,
	'errMsg'=>$errMsg,
	'exeTime'=> $testMode ? number_format((microtime(true) - $startTime), 4) : false,
	'query'=> $testMode ? $query : false
	), 
JSON_NUMERIC_CHECK);


?>
