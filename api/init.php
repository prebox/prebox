<?php

include_once(dirname(__FILE__)."/config.php");
include_once(dirname(__FILE__)."/_lib/groupFunctions.php");

$queryMain = "SELECT * from user where userId='$myUserId'";
$user = getdbquery($queryMain);

$group = getGroup($user['groupId']);
if($group) {
	$groups = getChildren($group);
	$groupType = $group['groupType'];
}


print json_encode(array(
	'churchId'=> 1,
	'user'=>$user,
	'group'=>$group,
	'groupType'=>$groupType,
	'groups'=>$groups,
	'query'=> $testMode ? $queryMain : false,
	'hasSetting'=>$admin['setting']['lan'] ? true : false
	), JSON_NUMERIC_CHECK);
?>